

use Cursist26;

DROP TABLE IF EXISTS EventTopic;

create table EventTopic(
    Id int(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name nvarchar(120)
    
);

DROP TABLE IF EXISTS EventCategory;

create table EventCategory(
    Id int(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(120)
    
);

DROP TABLE IF EXISTS Role;

create table Role(
    Id INT(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(50) 
    
);

DROP TABLE IF EXISTS Country;

create table Country(
    Id INT(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(30),
    Code NVARCHAR(3)
    
);