<?php
include('../template/header.php');
if(isset($_GET['Id'])){
    include('../../config.php');
    include('../../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    try {
        $sql = 'SELECT * FROM Country WHERE Id = :Id';
         // echo $sql;
        // return;
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        //$result = wat men retourneert moet een associatieve array zijn.

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
}
if(isset($_POST['submit'])){
    include('../../config.php');
    include('../../common.php');
        $NewCountry =  array(
            'Id' => escape($_POST['Id']),
            'Name' => escape($_POST['Name']),
            'Code' => escape($_POST['Code'])
            );
try{
        $sql = 'UPDATE Country set Name = :Name, Code = :Code WHERE Id = :Id';
        
        
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindparam(':Id', $Country['Id']);
        $statement->bindparam(':Name', $Country['Name']);
        $statement->bindparam(':Code', $Country['Code']);
        $statement->execute();
        $statement->execute($NewCountry);
            }
    catch (\PDOException $exception) {
        echo $sql . "</br>" . $exception->getMessage();
    }
    
}


?>

<div id="feedback">
    <?php
    if(isset($_POST['submit']) && $statement){
        echo "{$NewCountry['Name']} {$NewCountry['Code']} is gewijzigd.";
    }
    ?>
</div>
<h2>Land wijzigen</h2>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

    <input type="hidden" name="Id" id="Id"
        value="<?php echo escape($result['Id']); ?>">
<div>
    <label for="Name"> Name</label>
    <input type="text" name="Name" id="Name"
        value="<?php echo escape($result['Name']); ?>">
</div>

<div>
    <label for="Code">Code</label>
    <input type="text" name="Code" id="Code" 
        value="<?php echo escape($result['Code']); ?>">
</div>

<input type="submit" name="submit" value="Update"/>

</form>
<?php
include('../template/footer.php');
?>
