<?php
    include('../../config.php');
    include('../../common.php');
    $statement = false;
    try {
        $sql = 'SELECT * FROM Country';
         // echo $sql;
        // return;
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }

    include('../template/header.php');
?>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Code</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
<?php
    if ($result && $statement->rowCount() > 0) {
        foreach ($result as $row) {
?>
        <tr>
            <td><?php echo $row['Id'];?></td>
            <td><?php echo $row['Name'];?></td>
            <td><?php echo $row['Code'];?></td>
            <td><a href="update-single.php?Id=<?php echo $row['Id'];?>">Edit</a></td>
        </tr>        
<?php
        }
    }
    
?>
    </tbody>
</table>

<?php
    include('../template/footer.php');
?>