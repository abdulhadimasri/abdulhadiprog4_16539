<?php
    $list = \ModernWays\FricFrac\Dal\Event::readAll();
?>
<aside>
    <table>
        <?php
            if ($list) {
                foreach($list as $item) {
        ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?php echo $item['Id'];?>">-></a></td>
                    <td><?php echo $item['Name'];?></td>
                    <td><?php echo $item['Starts'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen events gevonden!!</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
