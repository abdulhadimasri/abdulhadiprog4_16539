
<?php
    include ('../template/header.php');
    $categoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
    $topicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
    $id = $_GET['Id'];
    // echo $id;
    $model = new \ModernWays\FricFrac\Model\Event();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Event::readOneById($id));

    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->setName($_POST['Name']);
        $model->setLocation($_POST['Location']);
        // bij insert wordt de id automatisch toegevoegd
        $model->setId($_POST['Id']);
        $model->setImage($_POST['Image']);
        $model->setStarts($_POST['StartDate'] . ' ' .$_POST['StartTime']);
        $model->setEnds($_POST['EndDate'] . ' ' .$_POST['EndTime']);
        $model->setDescription($_POST['Description']);
        $model->setOrganiserName($_POST['OrganiserName']);
        $model->setOrganiserDescription($_POST['OrganiserDescription']);
        $model->setEventCategoryId($_POST['EventCategoryId']);
        $model->setEventTopicId($_POST['EventTopicId']);        
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\Event::update($model->toArray());
        // eigenlijk moeten we naar de ReadingOne maar die
        // is nog niet klaar
        header("Location: Index.php");
    }
?>
<main>
    <article>
        <header>
            <h2>Event</h2>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <div>
                <label for="Name">Naam</label>
                <input type="text" required id="Name" name="Name" readonly
                    value="<?php echo $model->getName();?>"/>
            </div>
             <div>
                <label for="Location">Plaats</label>
                <input type="text" id="Location" name="Location" readonly
                    value="<?php echo $model->getLocation();?>"/>
            </div>
             <div>
                <label for="StartTime">Starttijd</label>
                <input type="time"  id="StartTime" name="StartTime" readonly
                    value="<?php echo $model->getStartTime();?>"/>
            </div>           
            <div>
                <label for="StartDate">Startdatum</label>
                <input type="date"  id="StartDate" name="StartDate" readonly
                    value="<?php echo $model->getStartDate();?>"/>
            </div>           
             <div>
                <label for="EndTime">Einde</label>
                <input type="time"  id="EndTime" name="EndTime" readonly
                    value="<?php echo $model->getEndTime();?>"/>
            </div>           
            <div>
                <label for="EndDate">Einddatum</label>
                <input type="date"  id="EndDate" name="EndDate" readonly
                    value="<?php echo $model->getEndDate();?>"/>
            </div>              <div>
                <label for="Image">Afbeelding</label>
                <input type="text"  id="Image" name="Image" readonly
                    value="<?php echo $model->getImage();?>"/>
            </div>     
             <div>
                <label for="Description">Beschrijving</label>
                <input type="text" required id="Description" name="Description" readonly
                    value="<?php echo $model->getDescription();?>"/>
            </div>    
             <div>
                <label for="OrganiserName">Naam organisator</label>
                <input type="text" required id="OrganiserName" name="OrganiserName" readonly
                    value="<?php echo $model->getOrganiserName();?>"/>
            </div>                
            <div>
                <label for="OrganiserDescription">Beschrijving organisator</label>
                <input type="text"  id="OrganiserDescription" name="OrganiserDescription" readonly
                    value="<?php echo $model->getOrganiserDescription();?>"/>
            </div>   
            <div>
                <label for="EventCategoryId">Event categorie</label>
                <select id="EventCategoryId" name="EventCategoryId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($categoryList) {
                        foreach ($categoryList as $row) {
                    ?>
                    <option value="<?php echo $row['Id'];?>" 
                        <?php echo $model->getEventCategoryId() === $row['Id'] ? 'SELECTED' : '';?>>
                        <?php echo $row['Name'];?>
                    </option>
                    <?php
                        }
                    }
                    ?>                
                </select>
            </div>
              <div>
                <label for="EventTopicId">Event topic</label>
                <select id="EventTopicId" name="EventTopicId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($topicList) {
                        foreach ($topicList as $row) {
                    ?>
                    <option value="<?php echo $row['Id'];?>"
                        <?php echo $model->getEventTopicId() === $row['Id'] ? 'SELECTED' : '';?>>
                        <?php echo $row['Name'];?>
                    </option>
                    <?php
                        }
                    }
                    ?>
                </select>
            </div>
                      
            </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template.footer.php');?>