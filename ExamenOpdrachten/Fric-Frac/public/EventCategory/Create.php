<?php
if(isset($_POST['submit'])){
     include('../../common.php');
     include('../../config.php');
     
     try{
         $connection = new \PDO($host,$username,$password,$options);
         $new_EventCategory = array(
             // eerst name in dit array komt van database veld Name
             "name" => escape($_POST["name"])
             );
             $sql = sprintf("insert into %s(%s) values(:%s)",
             "EventCategory",
             implode(", ",array_keys($new_EventCategory)),
             implode(", :",array_keys($new_EventCategory))
             
             );
             $statment = $connection->prepare($sql);
             $statment->execute($new_EventCategory);
         
     }catch(\PDOException $error){
         echo $sql . "<br/>" . $error->getMassage();
     }
}
?>


<?php include("../templates/header.php") ?>

<?php if(isset($_POST['submit']) && $statment){ ?>
<blockquote><?php echo escape($_POST['name']); ?> successfully added.</blockquote>
<?php
}
?>

<h2>Add a Event Category</h2>
<form action=<?php echo htmlentities($_SERVER['PHP_SELF']); ?> method="post">
   <label for="name">EventCategory Name</label>
	<input type="text" name="name" id="name">
	<button type="submit" name="submit" value="Create-EventCategory">Verznden</button>
</form>


<?php include("../templates/footer.php") ?>