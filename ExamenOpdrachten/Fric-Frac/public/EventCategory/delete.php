<?php
include('../templates/header.php');
include('../../config.php');
include('../../common.php');
if (isset($_GET["Id"])) {
  try {
    $connection = new PDO($host, $username, $password, $options);
  
    $Id = $_GET["Id"];

    $sql = "DELETE FROM EventCategory WHERE Id = :Id";

    $statement = $connection->prepare($sql);
    $statement->bindValue(':Id', $Id);
    $statement->execute();
  } catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}

try {
  $connection = new PDO($host, $username, $password, $options);

  $sql = "SELECT * FROM EventCategory";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>        
<h2>Delete EventCategory</h2>

<table>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($result as $row) : ?>
    <tr>
      <td><?php echo escape($row["Id"]); ?></td>
      <td><?php echo escape($row["Name"]); ?></td>
      <td><a href="delete.php?Id=<?php echo escape($row["Id"]); ?>">Delete</a></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<a href="../Index.php">Back to home</a>

<?php require "../templates/footer.php"; ?>
