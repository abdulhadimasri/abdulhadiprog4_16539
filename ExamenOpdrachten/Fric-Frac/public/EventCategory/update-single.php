<?php
include('../templates/header.php');
if(isset($_GET['Id'])){
    include('../../config.php');
    include('../../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    try {
        $sql = 'SELECT * FROM EventCategory WHERE Id = :Id';
         // echo $sql;
        // return;
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        //$result = wat men retourneert moet een associatieve array zijn.

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
}
if(isset($_POST['submit'])){
    include('../../config.php');
    include('../../common.php');
        $NewCategory =  array(
            'Id' => escape($_POST['Id']),
            'Name' => escape($_POST['Name'])
            );
try{
        $sql = 'UPDATE EventCategory set Name = :Name WHERE Id = :Id';
        
        
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindparam(':Id', $Category['Id']);
        $statement->bindparam(':Name', $Category['Name']);
        $statement->execute();
        $statement->execute($NewCategory);
            }
    catch (\PDOException $exception) {
        echo $sql . "</br>" . $exception->getMessage();
    }
    
}


?>

<div id="feedback">
    <?php
    if(isset($_POST['submit']) && $statement){
        echo "{$NewCategory['Name']} is gewijzigd.";
    }
    ?>
</div>
<h2>Land wijzigen</h2>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

    <input type="hidden" name="Id" id="Id"
        value="<?php echo escape($result['Id']); ?>">
<div>
    <label for="Name"> Name</label>
    <input type="text" name="Name" id="Name"
        value="<?php echo escape($result['Name']); ?>">
</div>

<input type="submit" name="submit" value="Update"/>

</form>
<?php
include('../templates/footer.php');
?>
