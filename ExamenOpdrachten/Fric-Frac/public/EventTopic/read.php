<?php
    // alleen uit te voeren als er op de submit knop is gedrukt
    if (isset($_POST['submit'])) {
        include('../../config.php');
        include('../../common.php');
        $Name = escape($_POST['Name']);
        $statement = false;
        try {
            $sql = 'SELECT * FROM EventTopic WHERE Name = :Name';
             // echo $sql;
            // return;
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->bindParam(':Name', $Name);
            $statement->execute();
            $result = $statement->fetchAll();

        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
    }

?>
<?php include "../templates/header.php"; ?>

<h2>Find Country based on name</h2>

<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    <div>
        <label for="Name">Name</label>
        <input type="text" id="Name" name="Name">
    </div>
    <button type="submit" name="submit" value="create-Country">Zoeken</button>
</form>

<table>
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            
        
    
<?php
    if (isset($_POST['submit'])) {
        if ($result && $statement->rowCount() > 0) {
            foreach ($result as $row) {
?>
                 <tr>
                     <td><?php echo $row['Name'];?></td>
                 </tr>   
<?php
            }
        }
    }

?>
    </tbody>
</table>
<?php
    include('../templates/footer.php');
?>