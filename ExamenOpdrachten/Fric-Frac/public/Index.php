<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <title>Beheer Fric-frac</title>
</head>
<body>
    <header><h1>Fric-frac</h1></header>
    <main>
        <a href="NewCountry/Index.php"class="tile"><span>Land</span></a>
        <a href="NewRole/Index.php"class="tile"><span>Rol</span></a>
        <a href="User/Index.php" class="tile"><span>Gebruiker</span></a>
        <a href="Event/Index.php" class="tile"><span>Event</span></a>
        <a href="NewEventCategory/Index.php" class="tile"><span>Event Categorie</span></a>
        <a href="NewEventTopic/Index.php"class="tile"><span>Event Topic</span></a>
</body>
</html>