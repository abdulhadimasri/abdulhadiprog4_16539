<?php
    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();
?>
<aside>
    <table class="table table-striped">
         <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Name</th>
      <th scope="col">Code</th>
    </tr>
  </thead>
        <?php
            if ($countryList) {
                foreach($countryList as $countryItem) {
        ?>
                <tr>
                    <td ><a class="btn btn-outline-primary" href="ReadingOne.php?Id=<?php echo $countryItem['Id'];?>">Edit</a></td>
                    <td><?php echo $countryItem['Name'];?></td>
                    <td><?php echo $countryItem['Code'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen landen gevonden</td></tr>
                <?php
            } 
        ?>
    </table>
</aside>
