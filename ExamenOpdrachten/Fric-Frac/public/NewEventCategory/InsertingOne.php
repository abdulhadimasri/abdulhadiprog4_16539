<?php
    include ('../template/header.php');
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\EventCategory();
        $model->setName($_POST['Name']);
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\EventCategory::create($model->toArray());
    }
?>
<main>
    <article>
        <header>
            <h2>EventCategory</h2>
        <nav>
            <button class="btn btn-light" type="submit" name="uc" value="insert" form="form">Insert</button>
            <a class="btn btn-warning" href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <div>
                <label for="Name">Naam</label>
                <input type="text" required id="Name" name="Name"/>
            </div>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template.footer.php');?>