<?php 
    $eventCategoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
?>
<aside>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
              <th scope="col"></th>
              <th scope="col">Name</th>
            </tr>
        </thead>
        <?php 
            if ($eventCategoryList) {
                foreach ($eventCategoryList as $eventCategoryItem) {
        ?>      
                <tr>
                    <td><a class="btn btn-outline-primary" href="ReadingOne.php?Id=<?php echo $eventCategoryItem['Id'];?>">Edit</a></td>
                    <td><?php echo $eventCategoryItem['Name'];?></td>
                </tr>
        <?php
                }
            }else {
        ?>
                <tr><td>Geen EventCategory gevonden</td></tr>
        <?php
            }
        ?>
    </table>
</aside>