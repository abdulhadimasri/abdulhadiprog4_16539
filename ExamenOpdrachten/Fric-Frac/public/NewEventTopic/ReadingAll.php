<?php
    $eventTopicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
?>
<aside>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
              <th scope="col"></th>
              <th scope="col">Name</th>
            </tr>
        </thead>
        <?php
            if ($eventTopicList) {
                foreach($eventTopicList as $eventTopicItem) {
        ?>
                <tr>
                    <td><a class="btn btn-outline-primary" href="ReadingOne.php?Id=<?php echo $eventTopicItem['Id'];?>">Edit</a></td>
                    <td><?php echo $eventTopicItem['Name'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen EventTopic gevonden</td></tr>
                <?php
            } 
        ?>
    </table>
</aside>
