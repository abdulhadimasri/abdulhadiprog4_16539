<?php
    $roleList = \ModernWays\FricFrac\Dal\Role::readAll();
?>
<aside>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
              <th scope="col"></th>
              <th scope="col">Name</th>
            </tr>
        </thead>
        <?php
            if ($roleList) {
                foreach($roleList as $roleItem) {
        ?>
                <tr>
                    <td><a class="btn btn-outline-primary" href="ReadingOne.php?Id=<?php echo $roleItem['Id'];?>">Edit</a></td>
                    <td><?php echo $roleItem['Name'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen Role gevonden</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
