<?php
    include ('../template/header.php');
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Role();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Role::readOneById($id));
    // var_dump($_POST);
    if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Role::delete($id);
            header("Location: Index.php");
       }
    }    
?>
<main>
    <article>
        <header>
            <h2>Role</h2>
        <nav>
            <a class="btn btn-secondary" href="UpdatingOne.php?Id=<?php echo $model->getId();?>">Updating</a>
            <a class="btn btn-light" href="InsertingOne.php">Inserting</a>
            <button class="btn btn-danger" type="submit" name="uc" value="delete" form="form">Delete</button>
           <a class="btn btn-warning" href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <div>
                <label for="Name">Naam</label>
                <input type="text" readonly id="Name" name="Name" 
                    value="<?php echo $model->getName();?>"/>
            </div>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template.footer.php');?>