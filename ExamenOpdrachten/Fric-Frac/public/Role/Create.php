<?php
if(isset($_POST['submit'])){
     include('../../common.php');
     include('../../config.php');
     
     try{
         $connection = new \PDO($host,$username,$password,$options);
         $new_Role = array(
             "name" => escape($_POST["Role_name"])
             );
             $sql = sprintf("insert into %s(%s) values(:%s)",
             "Role",
             implode(", ",array_keys($new_Role)),
             implode(", :",array_keys($new_Role))
             
             );
             $statment = $connection->prepare($sql);
             $statment->execute($new_Role);
         
     }catch(\PDOException $error){
         echo $sql . "<br/>" . $error->getMassage();
     }
}
?>


<?php include("../templates/header.php") ?>

<?php if(isset($_POST['submit']) && $statment){ ?>
<blockquote><?php echo escape($_POST['Role_name']); ?> successfully added.</blockquote>
<?php
}
?>

<h2>Add a Role</h2>
<form action=<?php echo htmlentities($_SERVER['PHP_SELF']); ?> method="post">
   <label for="Role_name">Role</label>
	<input type="text" name="Role_name" id="Role_name">
	<button type="submit" name="submit" value="Create-Role">Verznden</button>
</form>


<?php include("../templates/footer.php") ?>