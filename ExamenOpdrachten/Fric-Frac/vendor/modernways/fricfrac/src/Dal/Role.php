<?php
namespace ModernWays\FricFrac\Dal;

class Role extends Base {
    
    public static function delete($id) {
        $success = 0;
        if (self::connect()) {
            $Id = \ModernWays\Helpers::escape($id);
      
              try {
                $sql = 'DELETE FROM Role WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $Id);
                $statement->execute();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rij met id $id bestaat niet!";
                } else {
                    self::$message = "De rij met id $id is gedeleted!";
                    
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Fout: verwijderen mislukt!";
            }
         
        }
        return $success;
    }
    public static function create($post) {
        $success = false;
        if (self::connect()) {
            $newRole =  array(
                'Name' => \ModernWays\Helpers::escape($post['Name'])
                );
            try {
                $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)" , 'Role',
                    implode(', ', array_keys($newRole)), 
                    implode(', :', array_keys($newRole)));
                $statment = self::$connection->prepare($sql);
                $statment->execute($newRole);
                self::$message = 'Rij is toegevoegd';
                $success = true;
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Rij is niet toegevoegd";
            }
        }
        return $success;
    }
    
    public static function readOneByName($name) {
        $success =0;
        if (self::connect()) {
            $Name = \ModernWays\Helpers::escape($name);
            try {
                $sql = 'SELECT * FROM Role WHERE Name = :Name';
                $statment = self::$connection->prepare($sql);
                $statment->bindParam(':Name', $name, \PDO::PARAM_STR);
                $statment->execute();
                $result = $statment->fetchAll();
                $success = $statment->rowCount();
                if ($success == 0) {
                    self::$message = "Geen Event Role met de naam $name gevonden.";
                } else {
                    self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
                }
            }catch (\PDOException $exception) {
               self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
             }
            return $success;
        }
    }
    
    public static function readOneById($id) {
        if (self::connect()) {
            $Name = \ModernWays\Helpers::escape($id);
            try {
                $sql = 'SELECT * FROM Role WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $id, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetch(\PDO::FETCH_ASSOC);
                if ($result) {
                    self::$message = "De rij met de id $id is ingelezen.";
                } else {
                   self::$message = "De rij met de id $id is niet ingelezen.";
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Geen Role met de id $id gevonden.";
            }
        } 
        return $result;
    }
    
    public static function readAll() {
        $result = null;
        if (self::connect()) {
            try{
                $sql = 'SELECT * FROM Role';
                $statement = self::$connection->prepare($sql);
                $statement->execute();
                $result = $statement->fetchAll();
                self::$message = "Alle rijen van Role zijn ingelezen";
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "De table Role is leeg";
            }
        }
        return $result;
    }
    
    public static function Update($post) {
        $success = 0;
        if (self::connect()) {
            $updateRole= array(
                'Id'=> \ModernWays\Helpers::escape($post['Id']),
                'Name'=> \ModernWays\Helpers::escape($post['Name'])
                );
                
                try{
                    $sql = 'UPDATE Role SET Name = :Name WHERE Id = :Id';
                    $statement = self::$connection->prepare($sql);
                    $statement-> bindParam(':Id', $updateRole['Id']);
                    $statement-> bindParam(':Name', $updateRole['Name']);
                    $statement-> execute($updateRole);
                    $success = $statement->rowCount();
                    if ($success== 0) {
                        self::$message = "Het Role met de naam {$updateRole['Name']} is niet gevonden.";
                    }else{
                        self::$message = "Het Role met de naam {$updateRole['Name']} is geupdated.";
                    }
                }catch (\PDOException $exception) {
                    self::$message = "Het Role met de naam {$updateRole['Name']} is niet geupdated. Syntax error: {$exception->getMessage()}";
                }
        }
        return $success;
    }
    
    
    
    
    
}


?>