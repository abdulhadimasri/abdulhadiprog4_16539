<?php
include_once ('../Dal/Base.php');
include_once ('../Dal/Country.php');

if (\ModernWays\FricFrac\Dal\Base::connect('global')) {
    echo 'We zijn gelukkig, want het is gelukt.';
} else {
    echo 'We hebben het gekopieerd van Jef maar toch werkt het niet :(';
}
echo \ModernWays\FricFrac\Dal\Base::getMessage();

if (\ModernWays\FricFrac\Dal\Country::create(array('Name' => 'België', 'Code' => 'BE'))) {
    echo 'We zijn gelukkig, want het is gelukt.';
} else {
    echo 'Oeps er is iets fout gelopen';
}
echo \ModernWays\FricFrac\Dal\Country::getMessage();
