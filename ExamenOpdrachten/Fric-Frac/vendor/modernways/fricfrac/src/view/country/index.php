<?php
include('../../Dal/Base.php');
include('../../Dal/Country.php');
include_once('../../Helpers.php');

 $alldata =  ModernWays\FricFrac\Dal\Country::readAll("Country");
 
include('../../templates/header.php');
 ?>
    <link rel="stylesheet" href="../../css/style.css" type="text/css" />
        <div class = "container">
           
            <h1>Countries List :</h1>
             <a class="CreateLink" href="create.php">Create New Country</a>
  <table border ="1" style=" padding:10px; width:50%; text-align:center;border: 1px solid black;margin-">
        <thead>
        <tr>
            <th>Naam</th>
            <th>Code</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($alldata as $value) {
            ?>
            <tr>
                <td><?php echo $value['Name']; ?></td>
                <td><?php echo $value['Code']; ?></td>
                <td><a href="updatesingle.php?id=<?php echo $value['Id']; ?>">Edit</a></td>
                <td><a href="RemoveSingle.php?id=<?php echo $value['Id']; ?>">Delete</a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    </table>
<?php
    include('../../templates/footer.php');
    ?>