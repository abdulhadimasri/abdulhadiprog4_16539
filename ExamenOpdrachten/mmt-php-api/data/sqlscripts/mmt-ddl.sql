-- An Orm Apart -- Sunday 27th of January 2019 03:29:21 PM
-- 
SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
-- mode changes syntax and behavior to conform more closely to standard SQL.
-- It is one of the special combination modes listed at the end of this section.
SET GLOBAL sql_mode = 'ANSI';
-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS mmt;
USE `mmt`;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Curiosity
-- Created on Sunday 27th of January 2019 03:29:21 PM
-- 
DROP TABLE IF EXISTS `Curiosity`;
CREATE TABLE `Curiosity` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Name` NVARCHAR (255) NOT NULL,
	`Image` VARCHAR (255) NULL,
	`Comment` NVARCHAR (255) NULL,
	`Type` NVARCHAR (255) NULL,
	`Period` NVARCHAR (255) NULL,
	`Coordinates` VARCHAR (80) NULL,
	`Longitude` VARCHAR (25) NULL,
	`Latitude` VARCHAR (25) NULL,
	`Country` NVARCHAR (120) NULL,
	`Region` NVARCHAR (120) NULL,
	`Province` NVARCHAR (255) NULL,
	`City` NVARCHAR (255) NULL);

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Log
-- Created on Sunday 27th of January 2019 03:29:21 PM
-- 
DROP TABLE IF EXISTS `Log`;
CREATE TABLE `Log` (
	`UserName` NVARCHAR (50) NOT NULL,
	`Email` NVARCHAR (255) NOT NULL,
	`Role` NVARCHAR (50) NULL,
	`Longitude` CHAR (25) NOT NULL,
	`Latitude` CHAR (25) NOT NULL,
	`CuriosityName` NVARCHAR (255) NOT NULL,
	`CuriosityId` INT NOT NULL,
	`CuriosityLongitude` CHAR (255) NOT NULL,
	`CuriosityLatitude` CHAR (255) NOT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`InsertedOn` TIMESTAMP NOT NULL);

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Role
-- Created on Sunday 27th of January 2019 03:29:21 PM
-- 
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
	`Name` NVARCHAR (50) NOT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT uc_Role_Name UNIQUE (Name));

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE User
-- Created on Sunday 27th of January 2019 03:29:21 PM
-- 
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
	`Name` NVARCHAR (50) NOT NULL,
	`Salt` NVARCHAR (255) NULL,
	`HashedPassword` NVARCHAR (255) NULL,
	`RoleId` INT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT uc_User_Name UNIQUE (Name),
	CONSTRAINT fk_UserRoleId FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`));

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE CuriosityComment
-- Created on Sunday 27th of January 2019 03:29:21 PM
-- 
DROP TABLE IF EXISTS `CuriosityComment`;
CREATE TABLE `CuriosityComment` (
	`CuriosityId` INT NULL,
	`UserName` NVARCHAR (255) NOT NULL,
	`Comment` NVARCHAR (255) NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT fk_CuriosityCommentCuriosityId FOREIGN KEY (`CuriosityId`) REFERENCES `Curiosity` (`Id`));

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;

