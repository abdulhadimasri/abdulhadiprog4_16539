

use Cursist26;


create table EventTopic(
    Id int(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name nvarchar(120)
    
);


create table EventCategory(
    Id int(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(120)
    
);


create table Role(
    Id INT(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(50) 
    
);


create table Country(
    Id INT(15) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Name NVARCHAR(30),
    Code NVARCHAR(3)
    
);