<?php
include ('config.php');

try {
    // in php gebruiken we backslashes om namespaces aan te geven.
    // Eén backslash wil zeggen dat de klasse in de rootnamespace staat.
    $connection = new \PDO($dsn, $username, $password, $options);
	$sql = file_get_contents("data/sqlscripts/init.sql");
	$connection->exec($sql);
	
	echo "Database and table Users created successfully.";
} catch(\PDOException $error) {
	echo $sql . "<br>" . $error->getMessage();
}