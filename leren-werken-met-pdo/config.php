<?php

$username = 'root';
$databaseName = 'TaniaRascia';
// anders dan Tania, hier geven we ook de databasenaam mee
$host = "mysql:host=localhost:3306;dbname=$databaseName";
$password = '';
$options = array(
     PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);