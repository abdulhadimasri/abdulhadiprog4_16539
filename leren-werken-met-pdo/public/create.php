<?php include "templates/header.php";
// alleen uit te voeren als er op de submit knop is gedrukt
if (isset($_POST['submit'])) {
    include('../config.php');
    include('../common.php');
        $newUser =  array(
            'FirstName' => escape($_POST['FirstName']),
            'LastName' => escape($_POST['LastName']),
            'Email' => escape($_POST['Email']),
            'Age' => escape($_POST['Age']),
            'Location' => escape($_POST['Location'])
            );
            $statement = false;
            try{
        //$sql = 'INSERT INTO Users (FirstName, LastName, Email, Age, Location) values (:FirstName, :LastName, :Email, :Age, :Location)';
        $sql = $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Users',
        implode(', ', array_keys($newUser)),
        implode(', :', array_keys($newUser)));
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        //$statement->bindparam(':FirstName', $newUser['FirstName']);
        //$statement->bindparam(':LastName', $newUser['LastName']);
        //$statement->bindparam(':Email', $newUser['Email']);
        //$statement->bindparam(':Age', $newUser['Age']);
        //$statement->bindparam(':Location', $newUser['Location']);
        //$statement->execute();
        $statement->execute($newUser);
            }
    catch (\PDOException $exception) {
        echo $sql . "</br>" . $exception->getMessage();
    }
}
?>
<div id="feedback">
<?php
 
 
if (isset($_POST['submit']) && $statement){
    echo "{$newUser['FirstName']} {$newUser['LastName']} is toegevoegd.";
}

?>
</div>
<!-- form>(div>label+input[id][name])*5 -->
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    <div><label for="FirstName">FirstName</label><input type="text" id="FirstName" name="FirstName"></div>
    <div><label for="LastName">LastName</label><input type="text" id="LastName" name="LastName"></div>
    <div><label for="Age">Age</label><input type="text" id="Age" name="Age"></div>
    <div><label for="Email">Email</label><input type="Email" id="Email" name="Email"></div>
    <div><label for="Location">Location</label><input type="text" id="Location" name="Location"></div>
    <button type="submit" name="submit" value="create-person">send</button>
</form>
<?php include "templates/footer.php"; ?>