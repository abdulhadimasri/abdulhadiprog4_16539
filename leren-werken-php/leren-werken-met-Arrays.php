<?php 
include('../helpers.php');
$movies[0] = 'De witte';
$movies[1] = 'Game of thrones';
$movies[2] = 'Hercule poirot';
$movies[3] = 'upstairs downstairs';

$films = array(
    0 => 'De witte',
    1 => 'Game of thrones',
    2 => 'Hercule poirot',
    3 => 'upstairs downstairs'
    );
    
echo "Mijn lieveingsfilm is {$movies[2]} <br />";

echo "{$films[3]} is een film van het jaar stilletjes ... <br/>";

$movies[10]='Game Over';
echo "Mijn lieveingsfilm is {$movies[10]} <br />";

$adres = array('Voornaam' => 'Mo' , 'Faimilenaam' => 'Jansens' ,'straat'=> 'pompoenstraat', 'stad' => 'Antwerpen',
'PostCode' => '2000' );
echo "{$adres['Voornaam']} {$adres ['Faimilenaam']} woont in de {$adres ['straat']} in {$adres['PostCode']} in {$adres['stad']} <br/>";

echo 'De waarden in de adres array zijn: <br/>';

foreach ($adres as $element) {
    echo "{$element} <br/>";
}

foreach ($adres as $value => $value1) {
    echo "De sleutel is: {$value} en de waarde is: {$value1} <br/>";
}

echo 'De waarden van multidimensionele array zijn: <br/>';

$multi = array(
    "movies" => $movies,
    "films" => $films,
    "adres" => $adres);
    
    foreach ($multi as $key => $value2) {
        echo 'De sleutel is:' . $key . 'en de waarde is een array: <br/>';
        foreach($value2 as $subkey => $subvalue){
            echo "De sleutel is: {$subkey} en de waarde is: {$subvalue} <br/>";
        }
    };
    
// Array operatoren 
$adreskopie = $adres;
if ($adreskopie == $adres) {
    echo 'adreskopie is identiek aan adres';
}else{
    echo 'adreskopie is verschillend van adres';
}


$adreskopie[1] = 'Hannah';
if ($adreskopie ==$adres) {
 echo 'adreskopie is identiek aan adres';
}else{
    echo 'adreskopie is verschillend van adres <br/>';
}

$adresTotaal = $adres + $adreskopie;
printDictionary( $adresTotaal, 'adresTotaal:', 'red');

printArray($adres, 'sort array:', 'green');
//string functies

echo strtolower('DIT IS EEN TEST');