<?php

//LerenWerkenMetEcho();
//VerschillTussenByValueEnByReference();
//lerenwerkenmetconstanten();
//bizarrerieRekenkundigOperator();
variableVariable();

function VerschillTussenByValueEnByReference(){
    $voornaam='Anna';
    $familienaam='Thomas';
    $adresvoornaam=&$voornaam;
    echo "De voornaam is $voornaam <br/>";
    echo "Het adres van de voornaam is $adresvoornaam<br/>";
    $voornaam='Jan';
    echo "De voornaam is $voornaam <br/>";
    echo "Het adres van de voornaam is $adresvoornaam <br/>";
    $adresvoornaam ="Hannah";
    echo "De voornaam is $voornaam<br/>";
    echo "Het adres van de voornaam is $adresvoornaam <br/>";
}

function LerenWerkenMetEcho(){
/* 
*Enkelvoudig tekst 
*string interpolatie
*/
echo  'leren programmeren met PHP';
echo '<br/>';
echo 'Eerste les';
echo '<br/>';
$voornaam ='Mohammed';
$familienaam= 'El farisi';
echo "je volledig naam is:$voornaam $familienaam";

//nu met html en css
echo "<p> Je volledig naam is: <span style=\"color:red;\">$voornaam </span> <span style=\"color:green;\">$familienaam</span>";
}

function lerenwerkenmetconstanten(){
    define('PI', 3.14);
    define('MAX', 20);
    define('MIN', 2);
    //CONSTANTEN CONSTANTEN NIET GEBRUIKEN IN STRING INTERPOLATIE
    //DUS WEL CONCATENATIE
    echo 'De waarde va PI is' . constant('PI');
    $value= 100;
     if ($value >= constant('MIN') && $value <= constant('MAX')) {
        echo 'bingo';
    }else{
        echo  ' <br /> De ingevoerde waarde ligt niet tussen ' . 
        constant('MIN') . ' en ' . constant ('MAX'). '.';
    }
}

function bizarrerieRekenkundigOperator(){
    $int=10;
    //return int++
    return ++$int;
}

function variableVariable(){
    $voornaam='Mo';
    $familienaam = 'Ingelbrecht';
    $watwiljeTonen = 'voornaam';
    echo $watwiljeTonen;
    $watwiljeTonen= 'familienaam';
    echo $watwiljeTonen;
}
?>